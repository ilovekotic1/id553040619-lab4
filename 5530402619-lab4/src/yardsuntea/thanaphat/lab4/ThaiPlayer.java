package yardsuntea.thanaphat.lab4;

public class ThaiPlayer extends Player {
	
	/**
	 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
	 */
	
	private String SportName;
	
	ThaiPlayer(String name, int Day, int Month, int Year, double height, double weight){
		super (name,Day,Month,Year,height,weight);
	}
	
	public void setSportName(String s){
		SportName = s;
	}
	
	public String getSportName(){
		return SportName;
	}
	
	public String toString() {

		return "Player [ " + this.getName()+ " is " + calAge(bDay, bMonth, bYear)
				+ " years old, with weight = " + Weight + " and height = " + Height
				+ " ] \n [ nationallity = " 
				+ (getSportName() != null ? "Thai award = " + getSportName() : "Thai ]");
	}
}
