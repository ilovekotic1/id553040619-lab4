package yardsuntea.thanaphat.lab4;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Write by Thanaphat Yardsuntea StudentID 553040261-9 Sec 2
 */

public class Player {

	private String Name;
	protected int bDay, bMonth, bYear;
	protected double Weight, Height;
	int age;

	Player(String name, int Day, int Month, int Year, double height,
			double weight) {

		Name = name;
		bDay = Day;
		bMonth = Month;
		bYear = Year;
		Weight = weight;
		Height = height;
	}

	public void setWeight(double weight) {
		Weight = weight;
	}

	public double getWeight() {
		return Weight;
	}

	public void setHeight(double height) {
		Height = height;
	}

	public double getHeight() {
		return Height;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getName() {
		return Name;
	}

	public void setBD(int d, int m, int y) {
		bDay = d;
		bMonth = m;
		bYear = y;
	}

	public int calAge(int D, int M, int Y) {
		Calendar cal = new GregorianCalendar(Y, M, D);
		Calendar now = new GregorianCalendar();
		int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		if ((cal.get(Calendar.MONTH) > now.get(Calendar.MONTH))
				|| (cal.get(Calendar.MONTH) == now.get(Calendar.MONTH) && cal
						.get(Calendar.DAY_OF_MONTH) > now
						.get(Calendar.DAY_OF_MONTH))) {
			res--;
		}
		return res;
	}

	public String toString() {
		return "Player [ " + this.getName() + " is "
				+ calAge(bDay, bMonth, bYear) + " years old, with weight = "
				+ Weight + " and height = " + Height + " ]";
	}
}
